## whyred-user 9 PKQ1.180904.001 V12.0.3.0.PEICNXM release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: whyred
- Brand: xiaomi
- Flavor: whyred-user
- Release Version: 9
- Id: PKQ1.180904.001
- Incremental: V12.0.3.0.PEICNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: xiaomi/whyred/whyred:9/PKQ1.180904.001/V12.0.3.0.PEICNXM:user/release-keys
- OTA version: 
- Branch: whyred-user-9-PKQ1.180904.001-V12.0.3.0.PEICNXM-release-keys
- Repo: xiaomi_whyred_dump_1872


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
